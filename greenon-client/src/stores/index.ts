import { device } from "./device";
import { modal } from "./modal";

const useStore = () => {
  return { device, modal };
};

export default useStore;
