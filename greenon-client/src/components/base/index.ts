export { default as Header } from './Header';
export { default as MobileHeader } from './MobileHeader';
export { default as ContentHeader } from './ContentHeader';
export { default as Sider } from './Sider';
export { default as Footer } from './Footer';
export { default as BottomTab } from './BottomTab';
export { default as PageTemplate } from './PageTemplate';
