import React from 'react';
import styled from 'styled-components';

const Box = styled.div`
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  text-align: center;
  padding: 10px;
  border-bottom: 1px solid #f4f4f4;
`;

const SortDeviceModal = () => {
  return <div></div>;
};

export default SortDeviceModal;
