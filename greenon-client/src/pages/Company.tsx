import React from 'react';
import { PageTemplate } from '../components/base';

const Company = () => {
  return <PageTemplate headerTitle="회사 소개"></PageTemplate>;
};

export default Company;
